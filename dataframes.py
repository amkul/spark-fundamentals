nycFlights_df = spark.read.csv('/Volumes/DATA/spark-fundamentals/flights.csv', inferSchema = True, header = True)
nycFlights_df.show()
nycFlights_df.printSchema()
nycFlights_df.count()
nycFlights_df.select('flight', 'origin', 'dest').show()
nycFlights_df.describe('distance').show()

nycFlights_df.filter(nycFlights_df.distance=='17').show()

nycFlights_df.filter(nycFlights_df.origin=='EWR').show()

nycFlights_df.filter(nycFlights_df.day=='2').show()

nycFlights_df.where((nycFlights_df.arr_delay < '0') & (nycFlights_df.origin=='JFK') & (nycFlights_df.day=='7')).show()

nycFlights_df.registerTempTable('NYC_Flights')
sqlContext.sql('select * from NYC_Flights').show()
sqlContext.sql('select min(air_time) from NYC_Flights').show()
